<?php

namespace parser_toys;

class AllMatcher extends Parser
{
    /**
     * @var Parser[]
     */
    private $parsers;

    /**
     * @param Parser[] $parsers
     */
    public function __construct(array $parsers)
    {
        $this->parsers = $parsers;
    }

    /**
     * @param string $content
     * @param int    $offset
     *
     * @return Result
     */
    public function parse($content, $offset)
    {
        $success = true;
        $all_matched = [];

        foreach ($this->parsers as $parser) {
            $result = $parser->parse($content, $offset);

            if ($result instanceof Success) {
                $matched = $result->getMatched();

                $offset += strlen($matched);

                $all_matched[] = $result->getValue();
            } else {
                $success = false;

                break;
            }
        }

        if ($success) {
            return $this->success($all_matched);
        } else {
            return $this->failure("expected {$this}");
        }
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return implode("", $this->parsers);
    }
}
