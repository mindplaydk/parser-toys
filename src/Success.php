<?php

namespace parser_toys;

class Success
{
    /**
     * @var mixed
     */
    private $matched;

    /**
     * @var callable[]
     */
    private $productions;

    /**
     * @param mixed      $matched
     * @param callable[] $productions
     */
    public function __construct($matched, array $productions)
    {
        $this->matched = $matched;
        $this->productions = $productions;
    }

    /**
     * @return mixed
     */
    public function getMatched()
    {
        return $this->matched;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        $result = $this->matched;
        
        foreach ($this->productions as $production) {
            $result = call_user_func($production, $result);
        }
        
        return $result;
    }
}
