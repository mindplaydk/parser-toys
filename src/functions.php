<?php

namespace parser_toys;

/**
 * @param string $string
 *
 * @return StringMatcher
 */
function str($string)
{
    return new StringMatcher($string);
}

/**
 * @param Parser[] $parsers
 * 
 * @return AllMatcher
 */
function all_of(array $parsers)
{
    return new AllMatcher($parsers);
}

/**
 * @param Parser $optional
 * 
 * @return OptionalMatcher
 */
function optional(Parser $optional)
{
    return new OptionalMatcher($optional);
}

/**
 * @param Parser $element
 * 
 * @return AtLeastOnceMatcher
 */
function at_least_once(Parser $element)
{
    return new AtLeastOnceMatcher($element);
}
