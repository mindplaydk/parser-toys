<?php

namespace parser_toys;

class AtLeastOnceMatcher extends Parser
{
    /**
     * @var Parser
     */
    private $element;

    /**
     * @param Parser $element
     */
    public function __construct(Parser $element)
    {
        $this->element = $element;
    }

    /**
     * @param string $content
     * @param int    $offset
     *
     * @return Success|Failure
     */
    public function parse($content, $offset)
    {
        $all_matched = [];

        do {
            $result = $this->element->parse($content, $offset);

            if ($result instanceof Success) {
                $matched = $result->getMatched();

                $offset += strlen($matched);

                $all_matched[] = $result->getValue();
            }
        } while ($result instanceof Success);

        if (count($all_matched)) {
            return $this->success($all_matched);
        } else {
            return $this->failure("expected {$this}");
        }
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return "({$this->element})+";
    }
}
