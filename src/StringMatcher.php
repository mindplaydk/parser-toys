<?php

namespace parser_toys;

class StringMatcher extends Parser
{
    /**
     * @var string
     */
    private $string;

    /**
     * @param string $string
     */
    public function __construct($string)
    {
        $this->string = $string;
    }
    
    /**
     * @param string $content
     * @param int $offset
     *
     * @return Result
     */
    public function parse($content, $offset)
    {
        if (@substr_compare($content, $this->string, $offset, strlen($this->string), true) === 0) {
            return $this->success($this->string);
        } else {
            return $this->failure("expected {$this->string}");
        }
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->string;
    }
}
