<?php

namespace parser_toys;

use RuntimeException;

abstract class Parser
{
    /**
     * @var callable[]
     */
    private $productions = [];

    /**
     * @param string $content
     * @param int $offset
     *
     * @return Success|Failure
     */
    abstract public function parse($content, $offset);

    /**
     * @param callable $production
     * 
     * @return $this
     */
    public function apply(callable $production)
    {
        $this->productions[] = $production;

        return $this;
    }

    /**
     * @param string $content
     * 
     * @return mixed production
     */
    public function run($content)
    {
        $result = $this->parse($content, 0);
        
        if ($result instanceof Success) {
            return $result->getValue();
        }

        /**
         * @var Failure $result
         */
            
        throw new RuntimeException($result->getMessage());
    }
    
    /**
     * @param mixed $matched
     * 
     * @return Success
     */
    protected function success($matched)
    {
        return new Success($matched, $this->productions);
    }

    /**
     * @param string $message
     * 
     * @return Failure
     */
    protected function failure($message = null)
    {
        return new Failure($message ?: "expected {$this->__toString()}");
    }
    
    /**
     * @return string
     */
    abstract public function __toString();
}
