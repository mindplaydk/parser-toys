<?php

namespace parser_toys;

class OptionalMatcher extends Parser
{
    /**
     * @var Parser
     */
    private $optional;

    /**
     * @param Parser $optional
     */
    public function __construct(Parser $optional)
    {
        $this->optional = $optional;
    }

    /**
     * @param string $content
     * @param int    $offset
     *
     * @return Success|Failure
     */
    public function parse($content, $offset)
    {
        $result = $this->optional->parse($content, $offset);
        
        if ($result instanceof Success) {
            return $this->success($result->getValue());
        } else {
            return $this->success(null);
        }
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return "({$this->optional})?";
    }
}
