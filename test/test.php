<?php

namespace parser_toys;

use RuntimeException;

require dirname(__DIR__) . '/vendor/autoload.php';
require dirname(__DIR__) . '/src/functions.php';

/**
 * @param Success|Failure $result
 * @param mixed           $matched
 */
function is_success($result, $matched)
{
    ok($result instanceof Success, "expected Success", $result);

    if ($result instanceof Success) {
        eq($result->getMatched(), $matched);
    }
}

/**
 * @param Success|Failure $result
 * @param string          $message
 */
function is_failure($result, $message)
{
    ok($result instanceof Failure, "expected Error", $result);

    if ($result instanceof Failure) {
        eq($result->getMessage(), $message);
    }
}

test(
    'can create production',
    function () {
        eq(str('123')->run('123'), '123', 'it returns the matched result as the default production');

        eq(str('123')->apply('intval')->run('123'), 123, 'it invokes the production function');

        eq(str('123')->apply('intval')->apply(function ($value) {
            return $value - 100;
        })->run('123'), 23, 'can stack productions');

        expect(
            RuntimeException::class,
            'should throw on failure',
            function () {
                str('123')->run('abc');
            }
        );
    }
);

test(
    StringMatcher::class,
    function () {
        $p = str("ab");

        is_success($p->parse("ab", 0), "ab");
        is_success($p->parse("nab", 1), "ab");
        is_success($p->parse("aba", 0), "ab");

        is_failure($p->parse("a", 0), "expected ab");
        is_failure($p->parse("ac", 0), "expected ab");
    }
);

test(
    AllMatcher::class,
    function () {
        eq(all_of([str('a'), str('b')])->run('ab'), ["a", "b"]);
        eq(all_of([str('a'), str('b')])->apply('implode')->run('ab'), "ab");

        $p = all_of([str('a'), str('b')])->apply('implode');

        is_success($p->parse("ab", 0), ["a", "b"]);
        is_success($p->parse("nab", 1), ["a", "b"]);
        is_success($p->parse("aba", 0), ["a", "b"]);

        is_failure($p->parse("a", 0), "expected ab");
        is_failure($p->parse("ac", 0), "expected ab");
    }
);

test(
    OptionalMatcher::class,
    function () {
        is_success(optional(str('a'))->parse("a", 0), "a");
        is_success(optional(str('a'))->parse("ab", 0), "a");
        is_success(optional(str('a'))->parse("ba", 1), "a");
        is_success(optional(str('a'))->parse("b", 0), null);

        eq(optional(str('1'))->apply('intval')->run("1"), 1);
    }
);

test(
    AtLeastOnceMatcher::class,
    function () {
        is_success(at_least_once(str('a'))->parse("a", 0), ["a"]);

        is_success(at_least_once(str('a'))->parse("aa", 0), ["a", "a"]);

        is_success(at_least_once(str('a'))->parse("aab", 0), ["a", "a"]);

        is_success(at_least_once(str('a'))->parse("aab", 1), ["a"]);

        is_success(at_least_once(str('1')->apply('intval'))->parse("1", 0), [1]);

        is_success(optional(at_least_once(str('a')))->parse("b", 0), null);

        is_failure(at_least_once(str('a'))->parse("b", 0), "expected (a)+");

        eq(at_least_once(str('3')->apply('intval'))->apply('array_sum')->run("333"), 9);
    }
);

exit(run());
